import { Routes, RouterModule } from '@angular/router';

// guard
import {GuardsService} from './services/guards.service';

import {HomeComponent} from './components/todo/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {UsersComponent} from './components/todo/users/users.component';
import {TodoComponent} from './components/todo/todo.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';




const appRoutes: Routes = [

  { path: '',
    component: TodoComponent,
    canActivate: [ GuardsService ],
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'users', component: UsersComponent },
      { path: '', redirectTo: '/home', pathMatch: 'full' }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: PageNotFoundComponent }

];

export const APP_ROUTES = RouterModule.forRoot( appRoutes);
