import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UsersService } from './users.service';

@Injectable()
export class GuardsService implements CanActivate {

  constructor(
    private auth: UsersService,
    private router: Router
  ) { }

  canActivate() {

    if ( this.auth.authenticated() ) {
      console.log('El guard paso!');
       return true;
    }else {
      console.error('Bloqueado por el Guard');
      this.router.navigate(['/login']);
       return false;
    }
  }

}
