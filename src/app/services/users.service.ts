import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { URL_API } from '../config/config';

import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import {Task} from '../models/task';

@Injectable()
export class UsersService {


  user: User;
  token: string;

  constructor(
    public http: HttpClient,
    public router: Router
  ) {
    this.loadStorage();
  }

  authenticated() {
    return this.token.length > 5 ? true : false;

  }

  getUser() {
    return this.user;
  }

  getUsers() {

    const url = URL_API + '/api/users';

    return this.http.get<User[]>( url )
      .map( (resp) => {
        return resp;
      });

  }

  loadStorage() {

    if ( localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.user = JSON.parse( localStorage.getItem('user') );
    } else {
      this.token = '';
      this.user = null;
    }

  }

  saveStorage( message: string, status: string, token: string, user: User ) {

    localStorage.setItem('message', message );
    localStorage.setItem('status', status );
    localStorage.setItem('user', JSON.stringify(user) );
    localStorage.setItem('token', token );

    this.user = user;
    this.token = token;
  }

  logout() {
    this.user = null;
    this.token = '';

    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('message');
    localStorage.removeItem('status');

    this.router.navigate(['/login']);
  }

  singin( user: User ) {


    const url = URL_API + '/api/user/signin';
    return this.http.post( url, user )
      .map( (resp: any) => {
        console.log(resp);
        this.saveStorage( resp.message, resp.status, resp.token, resp.user );

        return true;
      });

  }


  createUser( user: User ) {

    const url = URL_API + '/api/user/register';

    return this.http.post( url, user )
      .map( (resp: any) => {
        return resp;
      });
  }

  deleteUser(id) {
    const url = URL_API + '/api/users/' + id;
    return this.http.delete( url )
      .map( (resp) => {
        console.log(resp);
        return resp;
      });
  }


}
