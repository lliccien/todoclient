import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {URL_API} from '../config/config';
import {Task} from '../models/task';
import * as url from 'url';

@Injectable()
export class TasksService {

  constructor( public http: HttpClient) { }

  getTasks() {

    const url = URL_API + '/api/tasks';

    return this.http.get<Task[]>( url )
      .map( (resp) => {
        return resp;
      });

  }

  getTask(id) {
    const url = URL_API + '/api/tasks/' + id;
    return this.http.get<Task>( url )
      .map(resp => {
        return resp;
      });
  }

  saveTask( task: Task ) {
    const url = URL_API + '/api/tasks';
    return this.http.post( url, task )
      .map( (resp) => {
        console.log(resp);
        return resp;
      });
  }

  updateTask(task: Task) {
    const url = URL_API + '/api/tasks/' + task.id;
    return this.http.put( url, task )
      .map( (resp) => {
        console.log(resp);
        return resp;
      });
  }

  deleteTask(id) {
    const url = URL_API + '/api/tasks/' + id;
    return this.http.delete( url )
      .map( (resp) => {
        console.log(resp);
        return resp;
      });
  }

}
