import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// routes
import {APP_ROUTES} from './app.routes';

// services
import {UsersService} from './services/users.service';
import {GuardsService} from './services/guards.service';
import {TokenInterceptorService} from './services/token-interceptor.service';

// components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/todo/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/todo/users/users.component';
import { NavbarComponent } from './components/todo/shared/navbar/navbar.component';
import { TodoComponent } from './components/todo/todo.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import {TasksService} from './services/tasks.service';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    UsersComponent,
    NavbarComponent,
    TodoComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [
   UsersService,
   GuardsService,
    TasksService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
