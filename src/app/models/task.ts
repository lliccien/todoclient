export class Task {

  constructor(
    public name: string,
    public priority: string,
    public deadline: string,
    public user_id?: string,
    public created_at?: string,
    public updated_at?: string,
    public id?: string
  ) { }

}
