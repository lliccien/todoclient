import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from '@angular/router';

import {UsersService} from '../../services/users.service';
import {User} from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = {
    email: null,
    password: null
  };
  constructor( private auth: UsersService,
               private route: Router) { }

  ngOnInit() {
  }

  login( form: NgForm) {
    if ( form.invalid ) {
      return;
    }
   const user = new User(form.value.email, form.value.password );
    console.log(user);
    this.auth.singin( user).subscribe( resp => {
      console.log(resp);
        this.route.navigate(['/home']);
    },
      error => {
        console.log(error);
      });
  }

}
