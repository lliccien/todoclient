import { Component } from '@angular/core';
import {TasksService} from '../../../services/tasks.service';
import {Task} from '../../../models/task';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {UsersService} from '../../../services/users.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  tasks: Task[] = [];
  task: Task;
  user_id = '';

  constructor( private _tasks: TasksService,
               private modalService: NgbModal,
               private auth: UsersService
               ) {
    this.getTasks();
    this.user_id = this.auth.getUser().id;
  }


  getTasks() {
    this._tasks.getTasks().subscribe( (resp: Task[] ) => {
      this.tasks = resp;
      this.tasks = this.alertDeadline(this.tasks);
      this.sortArrOfObjectsByParam(this.tasks, 'priority', false);
      });
  }

  open(modal, taskId) {
    if (taskId) {
      this.loadTask(taskId);
    }else {
      this.task = {
        name: null,
        priority: null,
        deadline: null
      };
    }
    setTimeout(() => {
      this.modalService.open(modal);
    }, 500);

  }

  newTask( form: NgForm) {
    if (form.invalid) {
      return;
    }
    const task = new Task(form.value.name, form.value.priority, form.value.deadline, this.user_id );
    this._tasks.saveTask( task ).subscribe( resp => {
      this.getTasks();
    });
  }

  loadTask(id) {
   /* aqui convierto la propiedad deadline de timestamo a formato yyyy-MM-dd */
   const task = this.tasks.find(function (obj) { return obj.id === id; });
   const timestamp = Number(task.deadline);
   const  date = new Date(timestamp);
   const  dead = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getDate();
   task.deadline = dead;
   this.task = task;
  /* AQui intento utilizar el meetodo getTask(id) del TasksServices y no funciona */
  //   this._tasks.getTask(id).subscribe((resp: Task) => {
  //     console.log(resp);
  //     return this.task = resp;
  //   });

  }

  editTask( form: NgForm) {
    if (form.invalid) {
      return;
    }
    console.log(form);
    const task = new Task(form.value.name, form.value.priority, form.value.deadline, this.user_id );

    this._tasks.updateTask( task ).subscribe( res => {
      this.getTasks();
    });

  }

  deleteTask(id) {
    this._tasks.deleteTask(id).subscribe( resp => {

      this.getTasks();
      return resp;
    });
  }


  sortArrOfObjectsByParam(arrToSort /* array */, strObjParamToSortBy /* string */, sortAscending /* bool(optional, defaults to true) */) {
    if (sortAscending == undefined) sortAscending = true;  // default to true

    if (sortAscending) {
      arrToSort.sort(function (a, b) {
        return a[strObjParamToSortBy] > b[strObjParamToSortBy];
      });
    }
    else {
      arrToSort.sort(function (a, b) {
        return a[strObjParamToSortBy] < b[strObjParamToSortBy];
      });
    }
  }

  private toTimestamp(strDate) {
    // console.log(strDate);
  return Date.parse(strDate);

}

// converts the deadline property to timestamp and
// creates a class property for tasks with expired deadline in red
// and for tasks that expire in a day in orange

  alertDeadline(arr) {
    const now = new Date();
    const twoDays = new Date();
    const dayOfMonth = twoDays.getDate();
    twoDays.setDate(dayOfMonth + 2);

    arr.forEach(item => {
      item.deadline = this.toTimestamp(item.deadline);
      if (item.deadline <= now) {
        item.class = 'list-group-item-danger';
      }else if (item.deadline <= twoDays) {
        item.class = 'list-group-item-warning';
      }
    });
    return arr;
  }

}
