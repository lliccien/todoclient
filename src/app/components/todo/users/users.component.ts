import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgForm} from '@angular/forms';
import {User} from '../../../models/user';
import {UsersService} from '../../../services/users.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  closeResult: string;
  user: User = {
    email: null,
    password: null,
    role: 'user'
  }
  user_id = '';
  constructor( private _user: UsersService,
               private modalService: NgbModal
               ) {
    this.getUsers();
    this.user_id = this._user.getUser().id;
  }

  ngOnInit() {
  }

  getUsers () {
    this._user.getUsers().subscribe( resp => {
      return this.users = resp;
    });
  }

  newUser( form: NgForm ) {
    if (form.invalid) {
      return;
    }
    const user = new User(form.value.email, form.value.password, form.value.role);
    this._user.createUser( user ).subscribe( resp => {
      this.getUsers();
      return resp;
    });

  }

  open(modaluser) {
    this.modalService.open(modaluser).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  deleteUser(id) {
    this._user.deleteUser(id).subscribe(resp => {
      this.getUsers();
      return resp;
    });
  }
}
