import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../../services/users.service';
import {User} from '../../../../models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: User;
  constructor( private auth: UsersService) {
    this.user = this.auth.getUser();
  }

  ngOnInit() {
  }
  logout() {
    this.auth.logout();
  }

}
